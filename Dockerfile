FROM golang:1.11
LABEL maintainer="zim"
ENV GOPATH /go
WORKDIR ${GOPATH}/src/api
COPY ./api .
RUN go get -d -v ./...
RUN go install -v ./...
CMD ["/go/bin/api"]