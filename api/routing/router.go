package routing

import (
	"api/controllers"
	"api/middleware"
	"github.com/gorilla/mux"
	"net/http"
)

func GetRouting() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/api/user/new", controllers.CreateAccount).Methods("POST")
	router.HandleFunc("/api/user/login", controllers.Authenticate).Methods("POST")
	router.HandleFunc("/api/user/posts", controllers.GetUserPosts).Methods("GET")
	router.HandleFunc("/api/posts", controllers.GetPosts).Methods("GET")
	router.HandleFunc("/api/posts", controllers.CreatePost).Methods("POST")
	router.HandleFunc("/api/posts/{id}", controllers.GetPost).Methods("GET")
	router.HandleFunc("/api/posts/{id}", controllers.DeletePost).Methods("DELETE")
	router.HandleFunc("/api/posts/{id}", controllers.UpdatePost).Methods("PUT")
	router.Use(middleware.Logger, middleware.Auth)
	router.NotFoundHandler = http.HandlerFunc(controllers.NotFound)
	return router
}
