package controllers

import (
	"api/functions"
	"api/models"
	"encoding/json"
	"net/http"
)

var CreateAccount = func(w http.ResponseWriter, r *http.Request) {
	account := &models.Account{}
	err := json.NewDecoder(r.Body).Decode(account)
	if err != nil {
		functions.Respond(w, functions.Message(false, "Invalid request"))
		return
	}
	resp := account.Create()
	functions.Respond(w, resp)
}

var Authenticate = func(w http.ResponseWriter, r *http.Request) {
	account := &models.Account{}
	err := json.NewDecoder(r.Body).Decode(account)
	if err != nil {
		functions.Respond(w, functions.Message(false, "Invalid request"))
		return
	}
	resp := models.Login(account.Email, account.Password)
	functions.Respond(w, resp)
}

var NotFound = func(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(404)
	functions.Respond(w, functions.Message(false, "Not Found"))
}