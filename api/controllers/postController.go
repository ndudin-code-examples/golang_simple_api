package controllers

import (
	"api/functions"
	"api/models"
	"encoding/json"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

var CreatePost = func(w http.ResponseWriter, r *http.Request) {
	post := &models.Post{}
	err := json.NewDecoder(r.Body).Decode(post)
	if err != nil {
		functions.Respond(w, functions.Message(false, "Invalid request"))
		return
	}
	ownerId, _ := primitive.ObjectIDFromHex(r.Context().Value("user").(string))
	post.OwnerId = ownerId.Hex()
	resp := post.Create()
	functions.Respond(w, resp)
}

var GetPosts = func(w http.ResponseWriter, r *http.Request) {
	post := &models.Post{}
	functions.Respond(w, post.GetAll(false))
}

var GetUserPosts = func(w http.ResponseWriter, r *http.Request) {
	post := &models.Post{}
	ownerId, _ := primitive.ObjectIDFromHex(r.Context().Value("user").(string))
	functions.Respond(w, post.GetUserPosts(ownerId,false))
}

var GetPost = func(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(vars["id"])
	post := &models.Post{}
	functions.Respond(w, post.Get(id, false))
}

var DeletePost = func(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(vars["id"])
	post := &models.Post{}
	err := json.NewDecoder(r.Body).Decode(post)
	if err != nil {
		functions.Respond(w, functions.Message(false, "Invalid request"))
		return
	}
	post.ObjectId = id
	functions.Respond(w, post.Delete())
}

var UpdatePost = func(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(vars["id"])
	post := &models.Post{}
	err := json.NewDecoder(r.Body).Decode(post)
	if err != nil {
		functions.Respond(w, functions.Message(false, "Invalid request"))
		return
	}
	post.ObjectId = id
	functions.Respond(w, post.Update())
}
