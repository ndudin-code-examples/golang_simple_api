package main

import (
	"api/routing"
	"log"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("APP_PORT")
	if port == "" {
		port = "8000"
	}
	log.Printf("Serving application on %s port" , port)
	err := http.ListenAndServe(":" + port, routing.GetRouting())
	if err != nil {
		log.Fatal(err)
	}
}