package db

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
)

var client *mongo.Client

func init() {
	port := os.Getenv("DB_PORT")
	host := os.Getenv("DB_HOST")
	log.Printf("Trying to connect to mongodb://%s:%s",host,port)

	clientOptions := options.Client().ApplyURI("mongodb://"+host+":"+port)

	client, _ = mongo.Connect(context.TODO(), clientOptions)

	err := client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	} else {
		log.Printf("Successfuly connected to mongodb://%s:%s",host,port)
	}
}

func GetDb() *mongo.Database{
	db := os.Getenv("DB_NAME")
	collection := client.Database(db)
	return collection
}
