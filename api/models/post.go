package models

import (
	"api/functions"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"time"
)

type Post struct {
	ObjectId   primitive.ObjectID `bson:"_id,omitempty"`
	Title      string
	Text       string
	OwnerId    string `bson:"accounts_id"`
	IsDeleted  bool   `bson:"is_deleted"`
	CreateTime time.Time
	UpdateTime time.Time
}

func (post *Post) Validate() (map[string]interface{}, bool) {
	if len(post.Title) == 0 {
		return functions.Message(false, "Title is required"), false
	}
	if len(post.Text) == 0 {
		return functions.Message(false, "Text is required"), false
	}
	return functions.Message(false, "Validate successful"), true
}

func (post *Post) Create() map[string]interface{} {
	if resp, ok := post.Validate(); !ok {
		return resp
	}
	post.IsDeleted = false
	post.CreateTime = time.Now()
	post.UpdateTime = time.Now()
	res, err := getCollection(PostsDb).InsertOne(context.TODO(), post)
	if err != nil {
		log.Print(err)
		return functions.Message(false, "Failed to create post, connection error.")
	} else {
		oid := res.InsertedID.(primitive.ObjectID)
		post.ObjectId = oid
		response := functions.Message(true, "Post has been created")
		response["post"] = post
		return response
	}
}

func (post *Post) Get(postId primitive.ObjectID, withDeleted bool) map[string]interface{} {
	var filter bson.M
	if withDeleted {
		filter = bson.M{"_id": postId}
	} else {
		filter = bson.M{"is_deleted": false, "_id": postId}
	}
	var item Post
	err := getCollection(PostsDb).FindOne(context.TODO(), filter).Decode(&item)
	if err != nil {
		log.Print(err)
	}
	response := functions.Message(true, "Post")
	response["post"] = item
	return response
}

func (post *Post) GetAll(withDeleted bool) map[string]interface{} {
	var filter primitive.M
	if withDeleted {
		filter = bson.M{}
	} else {
		filter = bson.M{"is_deleted": false}
	}
	return post.GetPosts(filter)
}

func (post *Post) GetUserPosts(userId primitive.ObjectID, withDeleted bool) map[string]interface{} {
	var filter primitive.M
	if withDeleted {
		filter = bson.M{"accounts_id": userId}
	} else {
		filter = bson.M{"is_deleted": false, "accounts_id": userId.Hex()}
	}
	return post.GetPosts(filter)
}

func (post *Post) GetPosts(filter primitive.M) map[string]interface{} {
	var posts []*Post
	cursor, err := getCollection(PostsDb).Find(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}
	for cursor.Next(context.TODO()) {
		var elem Post
		err := cursor.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		posts = append(posts, &elem)
	}
	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}
	_ = cursor.Close(context.TODO())
	response := functions.Message(true, "Posts")
	response["posts"] = posts
	return response
}

func (post *Post) Delete() map[string]interface{} {
	post.IsDeleted = true
	return post.Update()
}

func (post *Post) Update() map[string]interface{} {
	if resp, ok := post.Validate(); !ok {
		return resp
	}
	filter := bson.M{"_id" : post.ObjectId}
	post.UpdateTime = time.Now()
	update := bson.M{"$set" : bson.M{"title": post.Title, "text": post.Text, "updatetime": post.UpdateTime, "is_deleted": post.IsDeleted}}
	_, err := getCollection(PostsDb).UpdateOne(context.TODO(), filter, update, )
	if err != nil {
		log.Fatal(err)
	}
	var res Post
	_ = getCollection(PostsDb).FindOne(context.TODO(), filter).Decode(&res)
	response := functions.Message(true, "Post has been updated")
	response["post"] = res
	return response
}
