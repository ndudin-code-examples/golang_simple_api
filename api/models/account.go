package models

import (
	"api/functions"
	"context"
	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
	"log"
	"os"
	"strings"
	"time"
)

type Token struct {
	UserId string
	Username string
	CreatedAt time.Time
	ExpireAt time.Time
	jwt.StandardClaims
}

type Account struct {
	ObjectId primitive.ObjectID `bson:"_id,omitempty"`
	Email string
	Password string
	Token string
}

func (account *Account) Validate() (map[string] interface{}, bool) {

	if !strings.Contains(account.Email, "@") {
		return functions.Message(false, "Email address is required"), false
	}
	if len(account.Password) < 6 {
		return functions.Message(false, "Password is required"), false
	}
	filter := bson.D{{"email", account.Email}}
	var result Account
	err := getCollection(AccountsDb).FindOne(context.TODO(), filter).Decode(&result)
	if err != nil {
		return functions.Message(false, "Requirement passed"), true
	} else {
		return functions.Message(false, "Email address already in use by another user."), false
	}
}

func (account *Account) Create() map[string] interface{} {

	if resp, ok := account.Validate(); !ok {
		return resp
	}

	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(account.Password), bcrypt.DefaultCost)
	account.Password = string(hashedPassword)
	res, err := getCollection(AccountsDb).InsertOne(context.TODO(), account)
	if err != nil {
		log.Print(err)
		return functions.Message(false, "Failed to create account, connection error.")
	} else {
		oid := res.InsertedID.(primitive.ObjectID).Hex()
		created := time.Now()
		expire := created.AddDate(0, 0, 1)
		tk := &Token{UserId:oid,Username: account.Email, CreatedAt: created, ExpireAt: expire}
		token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
		tokenString, _ := token.SignedString([]byte(os.Getenv("SECRET")))
		account.Token = tokenString
		account.Password = ""
		account.ObjectId, _ = primitive.ObjectIDFromHex(oid)
		response := functions.Message(true, "Account has been created")
		response["account"] = account
		return response
	}
}

func Login(email, password string) map[string]interface{} {
	filter := bson.D{{"email", email}}
	var account Account
	err := getCollection(AccountsDb).FindOne(context.TODO(), filter).Decode(&account)
	if err != nil {
		return functions.Message(false, "Email address not found")
	} else {
		err = bcrypt.CompareHashAndPassword([]byte(account.Password), []byte(password))
		if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
			return functions.Message(false, "Invalid login credentials. Please try again")
		}
		account.Password = ""
		created := time.Now()
		expire := created.AddDate(0, 0, 7)
		tk := &Token{UserId:account.ObjectId.Hex(),Username: account.Email, CreatedAt: created, ExpireAt: expire}
		token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
		tokenString, _ := token.SignedString([]byte(os.Getenv("SECRET")))
		account.Token = tokenString

		resp := functions.Message(true, "Logged In")
		resp["account"] = account
		return resp

	}
}

func GetUser(id string) Account {
	filter := bson.M{"_id": id}
	var account Account
	err := getCollection(AccountsDb).FindOne(context.TODO(), filter).Decode(&account)
	if err != nil{
		log.Print(err)
	}
	return account
}