package models

import (
	"api/db"
	"go.mongodb.org/mongo-driver/mongo"
)

const AccountsDb string = "accounts"
const PostsDb string = "posts"

func getCollection(collection string) *mongo.Collection {
	return db.GetDb().Collection(collection)
}