package middleware

import (
	"api/functions"
	"api/models"
	"context"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"log"
	"net/http"
	"os"
	"time"
)

func Logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(fmt.Sprintf("URL: %s - METHOD: %s - ADDR: %s - USER_AGENT: %s",r.URL.RequestURI(),r.Method,r.RemoteAddr, r.UserAgent()))
		next.ServeHTTP(w, r)
	})
}

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		
			notAuth := []string{"/api/user/login"}
			requestPath := r.URL.Path

			for _, value := range notAuth {
				if value == requestPath {
					next.ServeHTTP(w, r)
					return
				}
			}

			response := make(map[string] interface{})
			tokenKey := r.Header.Get("Authorization")

			if tokenKey == "" {
				response = functions.Message(false, "Missing auth token")
				w.WriteHeader(http.StatusForbidden)
				w.Header().Add("Content-Type", "application/json")
				functions.Respond(w, response)
				return
			}

			if len(tokenKey) < 1 {
				response = functions.Message(false, "Invalid/Malformed auth token")
				w.WriteHeader(http.StatusForbidden)
				w.Header().Add("Content-Type", "application/json")
				functions.Respond(w, response)
				return
			}

			tk := &models.Token{}

			token, err := jwt.ParseWithClaims(tokenKey, tk, func(token *jwt.Token) (interface{}, error) {
				return []byte(os.Getenv("SECRET")), nil
			})

			if err != nil {
				response = functions.Message(false, "Malformed authentication token")
				w.WriteHeader(http.StatusForbidden)
				w.Header().Add("Content-Type", "application/json")
				functions.Respond(w, response)
				return
			}

			if !token.Valid {
				response = functions.Message(false, "Token is not valid.")
				w.WriteHeader(http.StatusForbidden)
				w.Header().Add("Content-Type", "application/json")
				functions.Respond(w, response)
				return
			}

			if tk.ExpireAt.Unix() < time.Now().Unix() {
				response = functions.Message(false, "Token is expired.")
				w.WriteHeader(http.StatusForbidden)
				w.Header().Add("Content-Type", "application/json")
				functions.Respond(w, response)
				return
			}

			ctx := context.WithValue(r.Context(), "user", tk.UserId)
			r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}